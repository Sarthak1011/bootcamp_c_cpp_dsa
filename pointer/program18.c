#include<stdio.h>
void main(){

	int x=10;
	int *iptr=&x;
	void *vptr=&x;

	printf("%d\n",*iptr);//10
	printf("%d\n",*((int*)vptr));//explicit type casting of void pointer

	printf("%p\n",iptr);//100
	printf("%p\n",vptr);//100
}
