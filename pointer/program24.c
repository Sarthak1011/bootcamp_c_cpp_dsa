//dangling pointer




#include<stdio.h>

	int a=10;
	int b;

	int *ptr=0;

	void fun(){
		int x=30;
		printf("%d\n",a);//10
		printf("%d\n",b);//0

		ptr=&x;

		printf("%p\n",ptr);//ox100
		printf("%d\n",*ptr);//30
	}
	void main(){

		int y=40;
		printf("%d\n",a);//10
		printf("%d\n",b);//0
		fun();
		printf("%d\n",*ptr);//30  dangling pointer 
	}



