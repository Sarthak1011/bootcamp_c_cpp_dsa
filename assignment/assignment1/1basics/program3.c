//write a program to print the first ten, 3 digit number

#include<stdio.h>
void main(){
	int num1,num2;
	printf("enter first 3 digit numbers and last number:\n");
	scanf("%d%d",&num1,&num2);
	printf("three digits numbers are:\n");
	for(int i=num1;i<=num2;i++){
		if(i>=100 && i<=999){
		printf("%d\n",i);
	}else{
		printf("invalid number\n");

}
}
}

/*
 output

 enter first 3 digit numbers and last number:
100
110
three digits numbers are:
100
101
102
103
104
105
106
107
108
109
110

 */
