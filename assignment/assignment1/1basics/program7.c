// write a program to print table of table 12

#include<stdio.h>
void main()
{
	int num1;
	printf("enter the number to make table of that number:\n");
	scanf("%d",&num1);
	printf("the table of %d is:\n",num1);
	for(int i=1;i<=10;i++){
		printf("%d * %d = %d \n",num1,i,i*num1);
         }
}

/*
 output

 enter the number to make table of that number:
12
the table of 12 is:
12 * 1 = 12
12 * 2 = 24
12 * 3 = 36
12 * 4 = 48
12 * 5 = 60
12 * 6 = 72
12 * 7 = 84
12 * 8 = 96
12 * 9 = 108
12 * 10 = 120
*/


