// write a program to print of table 11 in reverse order

#include<stdio.h>
void main()
{
	int num1;
	printf("enter the number to make table of that number:\n");
	scanf("%d",&num1);
	printf("the table of %d is:\n",num1);
	for(int i=10;i>=1;i--){
		printf("%d * %d = %d \n",num1,i,i*num1);
         }
}
/*
  output

 
  enter the number to make table of that number:
11
the table of 11 is:
11 * 10 = 110 
11 * 9 = 99 
11 * 8 = 88 
11 * 7 = 77 
11 * 6 = 66 
11 * 5 = 55 
11 * 4 = 44 
11 * 3 = 33 
11 * 2 = 22 
11 * 1 = 11
