//write a program to print the first 10 capital alphabets

#include<stdio.h>
void main(){
	char ch1;
	char ch2;
	printf("enter start and end value:\n");
	scanf("%c %c",&ch1,&ch2);
	for(char i=ch1;i<=ch2;i++)
	{
		printf("%c\n",i);
	}
}

/*output
 enter start and end value:
A
J

A
B
C
D
E
F
G
H
I
J
*/
