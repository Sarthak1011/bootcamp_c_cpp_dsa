//write a program to print the first 100 numbers


#include<stdio.h>
void main(){
	int num1;
	int num2;
	printf("enter first and last number is :\n");
	scanf("%d%d",&num1,&num2);
	printf("The number between %d and %d is :\n",num1,num2);
	for(int i=num1;i<=num2;i++)
	{
		printf("%d ",i);
	}
}

/*
 output

 enter first and last number is :
1
100
The number between 1 and 100 is :
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 */
