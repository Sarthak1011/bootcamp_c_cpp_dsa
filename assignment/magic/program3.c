/*
3. WAP to print all the composite numbers between a given range
Input : Enter Start 1
Enter End 15
Output:
4 6 8 9 10 12 14 15
Input : Enter Start 31
Enter End 35
Output:
32 33 34 35 
 */

#include<stdio.h>
void main(){
	int n1,n2,count=0;
	printf("enter the starting and ending number\n");
	scanf("%d%d",&n1,&n2);

	for(int i=n1;i<=n2;i++){
	int count=0;
	for(int j=1;j<=n2;j++){
		if(i%j==0){
			count++;
		}
	}
	if(count>2){
		printf("%d\t",i);
	}
	}
}

