/*
 1   2   3   4
 25  36  49  64
 9   10  11  12
 169 196 225 256

 */

#include<stdio.h>
void main(){
	int rows,cols;
	printf("enter the rows and cols\n");
	scanf("%d%d",&rows,&cols);
          int x=1;
	for(int i=1;i<=rows;i++){
		for(int j=1;j<=cols;j++){
		    if(i%2==1){
			    printf("%d\t",x);
			    x++;
		    }else{
			    printf("%d\t",x*x);
			    x++;
		    }
		}
		printf("\n");
		
	}
}

