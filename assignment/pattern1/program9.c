/*
 1  2  3
 a  b  c 
 1  2  3 
 a  b  c
 */

#include<stdio.h>
void main(){
	int rows,col;
	printf("enter the rows\n");
	scanf("%d",&rows);
	printf("enter the column\n");
	scanf("%d",&col);

	for(int i=1;i<=rows;i++){
		int x=1;
		char ch='a';
		for(int j=1;j<=col;j++){
			if(i%2==1){
				printf("%d\t",x);
			    x++;
			}else{
				printf("%c\t",ch);
				ch++;
			}
		}
		printf("\n");
	}
}

