#include<stdio.h>
void main(){
	int x=10;
	char ch='A';
	double d=11.11;

	void *arr[3]={&x,&ch,&d};// array of void pointer

	printf("%p\n",arr[0]);
	printf("%p\n",arr[1]);
	printf("%p\n",arr[2]);

	printf("%d\n",*(int *)arr[0]);//type casting void pointer
	printf("%c\n",*(char *)arr[1]);
	printf("%lf\n",*(double *)arr[2]);
}
