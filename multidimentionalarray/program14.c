// pointer to an array



#include<stdio.h>
void main(){
	int arr[]={1,2,3,4,5};


	int *ptr=arr;
	int *ptr1=&arr[2];
	int (*ptr2)[5]=&arr;//pointer to an array


	printf("%d\n",*ptr);//1
	printf("%d\n",*ptr1);//3
	printf("%d\n",**ptr2);//1
	
	printf("%p\n",ptr);//ox100
	printf("%p\n",ptr1);//ox108
	printf("%p\n",ptr2);//ox100
}

	
