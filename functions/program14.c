#include<stdio.h>
void swap(int *,int *);
void main(){
	int x=10;
	int y=20;
	printf("%d\n",x);//10
	printf("%d\n",y);//20
	swap(&x,&y);
	printf("%d\n",x);//20
	printf("%d\n",y);//10
}
void swap(int *ptr1,int *ptr2){
	printf("%d\n",*ptr1);//10
	printf("%d\n",*ptr2);//20

	int temp;
	temp=*ptr1;
	*ptr1=*ptr2;
	*ptr2=temp;
	printf("%d\n",*ptr1);//20
	printf("%d\n",*ptr2);//10
}
