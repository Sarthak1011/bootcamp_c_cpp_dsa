// WAP which will take 2 arrays from the user and compare beteen them.
// input:a1{1,2,3,4,5}
//       a2{4,3,2,1,7}
//       output : arrays are not equal
//

#include<stdio.h>
void main(){
	int size;
	printf("enter the array size\n");
	scanf("%d",&size);

	int arr1[size],arr2[size];

	printf("enter the first array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr1[i]);
	}

	printf("enter the second array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr2[i]);
	}


int flag;
	for(int i=0;i<size;i++){
		if(arr1[i]==arr2[i]){
			flag=1;
		}else{
			flag=0;
			break;
		}
	}
	if(flag==1){
		printf("arrays are equal\n");
	}else{
		printf("arrays are not equal\n");
	}
}
