//Write realtime one example for a singly linked list and draw a diagram as well. 

//Push code on gitlab.

//Happy Coding


#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct sportName{
	char sname[20];
	int pcount;
	float area;
	struct sportName *next;
}sp;

sp *head=NULL;

sp *createNode(){
	sp *Node=(sp *)malloc(sizeof(sp));
	getchar();
	printf("enter the sport name\n");
	fgets(Node->sname,20,stdin);
	Node->sname[strlen(Node->sname)-1]='\0';

	printf("enter the player count \n");
	scanf("%d",&Node->pcount);

	printf("enter the total area required\n");
	scanf("%f",&Node->area);

	Node->next=NULL;
	return Node;
}
void addNode(){
	sp *Node=createNode();

	if(head==NULL){
		head=Node;
	}else{
		sp *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=Node;
	}
}

void addFirst(){

	sp *Node=createNode();
	if(head==NULL){
		head=Node;
	}else{
		Node->next=head;
		head=Node;
	}
}
void addLast(){
	addNode();
}

int countNode(){
	sp *temp=head;
	int count=0;;
	while(temp!=NULL){
		temp=temp->next;
		count++;

	}
	return count;
}
int addAtPos(){
	int pos;
	printf("enter the position \n");
	scanf("%d",&pos);
	int count=countNode();
	if(pos<=0 || pos>=count+2){
		printf("wrong input\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addLast();
		}else{
			sp *temp=head;
			sp *Node=createNode();
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			Node->next=temp->next;
			temp->next=Node;
		}
	}
	return 0;
}
void printLL(){
	sp *temp=head;

	while(temp->next!=NULL){
		

		printf("|%s->",temp->sname);
		printf("%d->",temp->pcount);
		printf("%f|->",temp->area);
	
		temp=temp->next;
	}
		printf("|%s->",temp->sname);
		printf("%d->",temp->pcount);
		printf("%f| ",temp->area);
	
	printf("\n");
}
int deleteFirst(){
	if(head==NULL){
		printf("there is no node \n");
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			sp *temp=head;
			head=temp->next;
			free(temp);
		}
	}
	return 0;
}
int deleteLast(){
	if(head==NULL){
		printf("there is no node to delete\n");
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			sp *temp=head;
			while(temp->next->next!=NULL){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
	}
	return 0;
}

int deleteAtPos(){
	int count =countNode();
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);

	if(pos<=0 || pos>count){
		printf("there is no node to delete\n");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			sp *temp1=head,*temp2=head;
			while(pos-2){
				temp1=temp1->next;
				temp2=temp2->next;
				pos--;
			}
			temp1=temp1->next;
			temp2->next=temp2->next->next;
			free(temp1);
		}
	}return 0;
}

void main(){
	char choice;
	do {
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.deleteFirst\n");
		printf("6.deleteLast\n");
		printf("7.deleteAtPos\n");
		printf("8.printLL\n");

		printf("enter the choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addLast();
				break;
			case 4:
				addAtPos();
				break;
			case 5:
				deleteFirst();
				break;
			case 6:
				deleteLast();
				break;
			case 7:
				deleteAtPos();
				break;
			case 8:
				printLL();
				break;
			default :
			           printf("wrong choice\n");
		}

	   	printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='Y' || choice=='y');
}





