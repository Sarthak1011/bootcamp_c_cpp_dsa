//WAP for linked list of molls consisting of its name, number of shops & revenue connect 3 molls int the linked list & print their data
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct moll{
	char name[20];
	int NOS;
	float revenue;
	struct moll *next;
}moll;
moll *head=NULL;

void addNode(){
	moll *node=(moll *)malloc (sizeof(moll));

	getchar();
	printf("enter the moll name\n");
	fgets(node->name,20,stdin);

	node->name[strlen(node->name)-1]='\0';

	printf("enter the number of shops\n");
	scanf("%d",&node->NOS);

	printf("enter the revenue\n");
	scanf("%f",&node->revenue);

//	getchar();

	node->next=NULL;

	if(head==NULL){
		head=node;
	}else{
		moll *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
void printLL(){
	moll *temp=head;
	while(temp!=NULL){
		printf("|%s->",temp->name);
		printf("%d->",temp->NOS);
		printf("%f|",temp->revenue);
		temp=temp->next;
	}
}
void main(){
	int call;
	printf("enter the call\n");
	scanf("%d",&call);
//	getchar();
	for(int i=0;i<call;i++){
	addNode();
	}
	printLL();
}
