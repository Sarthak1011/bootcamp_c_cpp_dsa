//WAP for the linked list of states in india consisting of it name,population,budget,litercy connet 4 states int the linked list and print it
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct state{
	char name[20];
	long pop;
	double budget;
	float litercy;
	struct state *next;
}state;

state *head=NULL;

void addNode(){
	state *node=(state *)malloc(sizeof(state));
	printf("enter the state name\n");
	fgets(node->name,20,stdin);
	node->name[strlen(node->name)-1]='\0';

	printf("enter the population of state\n");
	scanf("%ld",&node->pop);

	printf("enter the budget of state\n");
	scanf("%lf",&node->budget);

	printf("enter the litercy of state\n");
	scanf("%f",&node->litercy);
//	getchar();

	node->next=NULL;

	if(head==NULL){
		head=node;
	}else{
		state *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
void printLL(){
	state *temp=head;
	while(temp!=NULL){
		printf("|%s->",temp->name);
		printf("%ld->",temp->pop);
		printf("%f->",temp->budget);
		printf("%f|",temp->litercy);
		temp=temp->next;

	}
}
void main(){
	int call;
	printf("enter the call\n");
	scanf("%d",&call);
	getchar();
	for(int i=0;i<call;i++){
		addNode();
	}
	printLL();
}

