//
#include<stdio.h>

int goodPair(int arr[],int size,int key){
	int count=-1;
	for(int i=0;i<size;i++){
		for(int j=i+1;j<size;j++){
			if(arr[i]+arr[j]==key){
				count++;
			}
		}
	}
	return count;
}
void main(){
	int size;
	printf("enter the array size\n");
	scanf("%d",&size);

	printf("enter the array elements\n");
	int arr[size];

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int sum;
	printf("enter the sum element\n");
	scanf("%d",&sum);

	int sumcount=goodPair(arr,size,sum);

	if(sumcount==-1){
		printf("no sum is detected\n");
	}else{
		printf("count of sum is %d\n",sumcount);
	}
}

