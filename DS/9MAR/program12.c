/*
 Q8: Row sum
Problem Description :
- You are given a 2D integer matrix A, and return a 1D integer array containing
row-wise sums of the original matrix.
- Return an array containing row-wise sums of the original matrix.
Input 1:
[1,2,3,4]
[5,6,7,8]
[9,2,3,4]
Output 1:
[10,26,18]
Explanation :
Row 1 = 1+2+3+4 = 10
Row 2 = 5+6+7+8 = 26
Row 3 = 9+2+3+4 = 18

 */ 
#include<stdio.h>

void main(){

        int row,col;
        printf("enter the row and col\n");
        scanf("%d %d",&row,&col);

        int arr[row][col];

        printf("array elements are\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        scanf("%d",&arr[i][j]);
                }
        }
        printf("array elements are\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        printf("%d\t",arr[i][j]);
                }
                printf("\n");
        }
	

	for(int i=0;i<row;i++){
	
		int sum=0;
		for(int j=0;j<col;j++){
			sum=sum+arr[i][j];
		}
		printf("%d\t",sum);

	}
	printf("\n");
}

		
