/*
Q6.
Given an Integer array of size of N.
Count the number of elements having at least 1 element greater than itself.
Input:
int arr[] = {2,5,1,4,8,0,8,1,3,8};
Output:
7
========================================================================
=========

 */
#include<stdio.h>


void main(){

        int size;

        printf("enter the array size\n");
        scanf("%d",&size);

        int arr[size];

        printf("enter the array elements \n");

        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("array elements are\n");

        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
        printf("\n");


	int count=0;
	int temp=0;
	int max=arr[0];
	for(int i=1;i<size;i++){
		if(max<arr[i]){
			max=arr[i];
		}
	}
	for(int i=0;i<size;i++){
			if(max==arr[i]){
				count++;
			}

		}
	printf("%d\n",size-count);
}


