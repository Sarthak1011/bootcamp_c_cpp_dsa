/*
 Q15. Add the matrices
Problem Description:
- You are given two matrices A & B of the same size, you have to return
another matrix which is the sum of A and B.
Example Input :
Input :
A = [[1, 2, 3],
[4, 5, 6],
[7, 8, 9]]
B = [[9, 8, 7],
[6, 5, 4],
[3, 2, 1]]
Output :
[[10, 10, 10],
[10, 10, 10],
[10, 10, 10]]
Example Explanation:
A + B = [[1+9, 2+8, 3+7],[4+6, 5+5, 6+4],[7+3, 8+2, 9+1]] = [[10, 10, 10],
[10, 10, 10], [10, 10, 10]].
========================================================================
=======
 */ 

#include<stdio.h>

void main(){

        int row,col;
        printf("enter the row and col\n");
        scanf("%d %d",&row,&col);

        int arr1[row][col];
        int arr2[row][col];

        printf("enter the array1 elements \n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        scanf("%d",&arr1[i][j]);
                }
        }
        printf(" array1 elements are\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        printf("%d\t",arr1[i][j]);
                }
                printf("\n");
        }
        printf("enter array2 elements\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        scanf("%d",&arr2[i][j]);
                }
        }
        printf("array2 elements are\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        printf("%d\t",arr2[i][j]);
                }
                printf("\n");
        }
        int flag=0;
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
			arr1[i][j]+=arr2[i][j];
		}
	}
	printf("after addition\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        printf("%d\t",arr1[i][j]);
                }
                printf("\n");
        }

} 
