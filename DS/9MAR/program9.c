/*
 Q5. Array Rotation
Problem Description :
- Given an integer array A of size N and an integer B, you have to return
the same array after rotating it B times towards the right.
- Return the array A after rotating it B times to the right
Problem Constraints :
1 <= N <= 105
1 <= A[i] <=109
1 <= B <= 109
Example Input :
Input 1:
A = [1, 2, 3, 4]
B = 2
Output 1:
[3, 4, 1, 2]
Input 2:
A = [2, 5, 6]
B = 1
Output 2:
[6, 2, 5]
Example Explanation :
Explanation 1:
Rotate towards the right 2 times
[1, 2, 3, 4] => [4, 1, 2, 3] => [3, 4, 1, 2]
Explanation 2:
Rotate towards the right 1 time
[2, 5, 6] => [6, 2, 5]
========================================================================
========

 */


#include<stdio.h>


void main(){

        int size;

        printf("enter the array size\n");
        scanf("%d",&size);

        int arr[size];

        printf("enter the array elements \n");

        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("array elements are\n");

        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
        printf("\n");

	int num;
	printf("enter the number how many times you rotate array\n");
	scanf("%d",&num);

	for(int i=0;i<num;i++){

		int last=arr[size-1];

		for(int j=size-1;j>0;j--){

			arr[j]=arr[j-1];
		}
		arr[0]=last;
	}
        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
        printf("\n");
}
