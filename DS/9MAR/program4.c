/*
 Q2. Good Pair
Problem Description :
- Given an array A and an integer B.
- A pair(i, j) in the array is a good pair if i != j and (A[i] + A[j] == B).
- Check if any good pair exist or not.
- Return 1 if good pair exist otherwise return 0.
Example Input
Input 1:
A = [1,2,3,4]
B = 7
Output 1:
1
Input 2:
A = [1,2,4]
B = 4
Output 2:
0
Input 3:
A = [1,2,2]
B = 4
Output 3:
1
Example Explanation :
Explanation 1:
(i,j) = (3,4)
Explanation 2:
No pair has a sum equal to 4.
Explanation 3:
(i,j) = (2,3)
 */ 



#include<stdio.h>


void main(){


	
        int size;

        printf("enter the array size\n");
        scanf("%d",&size);

        int arr[size];

        printf("enter the array elements \n");

        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("array elements are\n");

        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
	printf("\n");

	int num;

	printf("enter the number \n");
	scanf("%d",&num);

	int count=0;
	for (int i=0;i<size;i++){
		for(int j=i+1;j<size;j++){
		if(arr[i]+arr[j]==num){
				count++;
			}
		}
	}
	printf("count is %d\n",count);
}
