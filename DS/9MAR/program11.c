/*
 Q7. Column Sum
Problem Description :
- You are given a 2D integer matrix A, and return a 1D integer array containing
column-wise sums of the original matrix.
- Return an array containing column-wise sums of the original matrix.
Input :
[1,2,3,4]
[5,6,7,8]
[9,2,3,4]
Output :
{15,10,13,16}
Example Explanation
Column 1 = 1+5+9 = 15
Column 2 = 2+6+2 = 10
Column 3 = 3+7+3 = 13
Column 4 = 4+8+4 = 16
========================================================================
========
 */ 

#include<stdio.h>

void main(){

	int row,col;
	printf("enter the row and col\n");
	scanf("%d %d",&row,&col);

	int arr[row][col];

	printf("array elements are\n");
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	printf("array elements are\n");
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			printf("%d\t",arr[i][j]);
		}
		printf("\n");
	}
	int colSum[col];
	for(int i=0;i<row;i++){
		int sum=0;
		for(int j=0;j<col;j++){
			 sum=sum+arr[j][i];
		}
	
		printf("%d\t",sum);
	}
	printf("\n");

}
