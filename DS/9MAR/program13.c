/*
 Q9: Main Diagonal sum
Problem Description :
- You are given a N X N integer matrix.
- You have to find the sum of all the main diagonal elements of A.
- The main diagonal of a matrix A is a collection of elements A[i, j]
such that i = j.
- Return an integer denoting the sum of main diagonal elements.
Input 1:
3 3 1 -2 -3 -4 5 -6 -7 -8 9
Output 1:
15
Input 2:
2 2 3 2 2 3
Output 2:
6
Example Explanation :
Explanation 1:
A[1][1] + A[2][2] + A[3][3] = 1 + 5 + 9 = 15
Explanation 2:
A[1][1] + A[2][2] = 3 + 3 = 6
========================================================================
=======
*/
#include<stdio.h>

void main(){

        int row;
        printf("enter the row and col\n");
        scanf("%d",&row);

        int arr[row][row];

        printf("array elements are\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<row;j++){
                        scanf("%d",&arr[i][j]);
                }
        }
        printf("array elements are\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<row;j++){
                        printf("%d\t",arr[i][j]);
                }
                printf("\n");
        }

	int sum=0;
	for(int i=0;i<row;i++){
		for(int j=0;j<row;j++){
			if(i==j){
				sum+=arr[i][j];
			}
		}
	}
	printf("%d\n",sum);
}

