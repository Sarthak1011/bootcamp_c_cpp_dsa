/*
 Q16. Row to column zero
Problem Description:
- You are given a 2D integer matrix A, make all the elements in a row or
column zero if the A[i][j] = 0.
- Specifically, make the entire ith row and jth column zero.
Input:
[1,2,3,4]
[5,6,7,0]
[9,2,0,4]
Output 1:
[1,2,0,0]
[0,0,0,0]
[0,0,0,0]
Explanation :
A[2][4] = A[3][3] = 0, so make 2nd row, 3rd row, 3rd column and 4th column zero.
========================================================================
==

 */ 


#include<stdio.h>

void main(){

        int row,col;
        printf("enter the row and col\n");
        scanf("%d %d",&row,&col);

        int arr[row][col];
        int arr2[row][col];

        printf("enter the array1 elements \n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        scanf("%d",&arr[i][j]);
                }
        }
        printf(" array1 elements are\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        printf("%d\t",arr[i][j]);
                }
                printf("\n");
        }
        
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			if(arr[i][j]==0){

				for(int k=0;k<col;k++){
					arr[k][j]=0;
				}
				for(int m=0;m<row;m++){
					arr[i][m]=0;
				}
			}
		}
	}

	printf("result\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        printf("%d\t",arr[i][j]);
                }
                printf("\n");

}
}
