//doublyLL


#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	struct demo *prev;
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	node->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();
	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
	}
}

void addfirst(){
	demo *node=createnode();
	if(head==NULL){
		head=node;
	}else{
		node->next=head;
		head->prev=node;
		head=node;
	}
}
void addlast(){
	addnode();
}
int countnode(){
	int count=0;
	demo *temp=head;
	while(temp!=NULL){
		count++;
	       temp=temp->next;
	}
return count;
}
int addatpos(){
	int count =countnode();
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);
	if(pos<=0||pos>=count+2){
		printf("wrong position\n");
		return -1;
	}else {
	
		if(pos==1){
			addfirst();
		}else if(pos==count+1){
			addlast();
		}else{
			demo *node=createnode();
			demo *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			node->next=temp->next;
			node->prev=temp;
			temp->next=node;
			temp=node;
		}
		
	}
	return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}
int deletefirst(){
	if(head==NULL){
		printf("linked list is emppty\n");
		return -1;
	}else{
		head=head->next;
		free(head->prev);
		head->prev=NULL;
	}
	return 0;
}
int deletelast(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next->next!=NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
	return 0;
}
int deleteatpos(){
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);
	int count=countnode();

	if(pos<=0||pos>count){
		printf("linked list is empty\n");
		return 0;
	}else{
		if(pos==1){
			deletefirst();
		}else if(pos==count){
			deletelast();
		}else{
			demo *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->next);
			temp->next->prev=temp;
		}
	}
	return 0;
}

			

void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.addfirst\n");
		printf("3.addlast\n");
		printf("4.addatpos\n");
		printf("5.printLL\n");
		printf("6.deletefirst\n");
		printf("7.deletelast\n");
		printf("8.deleteatpos\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				addfirst();
				break;
			case 3:
				addlast();
				break;
			case 4:
				addatpos();
				break;
			case 5:
				printLL();
				break;
			case 6:
				deletefirst();
				break;
			case 7:
				deletelast();
				break;
			case 8:
				deleteatpos();
				break;
			default :
			            printf("wrong choice\n");
		}

		printf("do you want continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}






