//doubly linked list
#include<stdio.h>
#include<stdlib.h>//malloc prototype is void * malloc (size_t)

typedef struct demo{
	struct demo *prev;
	int data;
	struct demo *next;
}demo;
demo *head=NULL;
demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	node->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){

	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
	}
}
void addfirst(){
	demo *node=createnode();
	if(head==NULL){
		head=node;
	}else{
		node->next=head;
		head->prev=node;
		head=node;
	}
}
void addlast(){
	addnode();
}

int countnode(){
	demo *temp=head;
	int count=0;
	while(temp!=NULL){
		temp=temp->next;
		count++;
	}
	return count;
}
void addatpos(){

	int count=countnode();
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);



	if(pos<=0 || pos>=count+2){
		printf("wrong position\n");

	}else{
		if(pos==1){
			addfirst();
		}else if(pos==count+1){
			addnode();
		}else{
			demo *temp=head;
			demo *node=createnode();
			while(pos-2){
			
				temp=temp->next;
				pos--;
			}
			node->next=temp->next;
			node->prev=temp;
			temp->next->prev=node;
			temp->next=node;
			
		}
		
	}

}

void printLL(){
	demo *temp=head;
	while(temp->next !=NULL){
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("|%d|\n",temp->data);
}


void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.addfirst\n");
		printf("3.addlast\n");
		printf("4.addatpos\n");
		printf("5.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addnode();
				break;
			case 2:
				addfirst();
				break;
			case 3:
				addlast();
				break;
			case 4:
				addatpos();
				break;
				
		        case 5:
				printLL();
				break;
				
                       default:
		                printf("wrong choice\n");
		}

                        printf("do you want to continue\n");
			getchar();
			scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}

				

	
