/*
 Program 3. Write a program that accepts two singly linear linked lists
from the user and concat the first N elements of the source linked list
after the destination linked list.
Input source linked list : |30|->|30|->|70|
Input destination linked list : |10|->|20|->|30|->|40|
Input number of elements : 2
Output destination linked list : |10|->|20|->|30|->|40|->|30|-
>|30|
 */ 


#include<stdio.h>
#include<stdlib.h>


typedef struct Node{
	int data;
	struct Node *next;
}node;

int flag=0;
node *head1=NULL;
node *head2=NULL;

node *createNode(){
	node *newNode=(node *)malloc(sizeof(node));
	printf("enter the data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(node **head){
	node *newNode=createNode();
	if(*head==NULL){
		*head=newNode;
	}else{
		node *temp=*head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(node **head){
	node *temp=*head;
	while(temp->next!=NULL){
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
		printf("|%d|\n",temp->data);
}
int concat(int num){
	if(head1==NULL && head2==NULL){
		flag=0;
		return -1;
	}else if(head1==NULL){
		printLL(&head2);
		flag=1;
	}else if(head2==NULL){
		printLL(&head1);
		flag=2;
	}else{
		node *temp1=head2;
		while(temp1->next!=NULL){
			temp1=temp1->next;
		}
		node *temp2=head1;
		while(num){
			temp2=temp2->next;
			num--;
		}
		temp2->next=temp1;
		flag=3;
		printLL(&head2);
	}
	return 0;
}
void main(){
        char choice;
        do {

                printf("1.addNode for source linked list\n");
                printf("2.addNode for destination linked list\n");
                printf("3.concat\n");
                printf("4.printLL\n");

                printf("enter your choice\n");
                int ch;
                scanf("%d",&ch);

                switch(ch){

                        case 1:
                                //printf("enter the input source linked list elements\n");
                                addNode(&head1);
                                printLL(&head1);
                                break;
                        case 2:
                                //printf("enter the input destination linked list elements\n");
                                addNode(&head2);
                                printLL(&head2);
                                break;
                        case 3:
                                {
					int num;
					printf("enter the number\n");
					scanf("%d",&num);

                                concat(num);
                                if(flag==0){
                                        printf("both linked list are empty\n");
                                }else if(flag==1){
                                        printf("source linked list is empty\n");
                                }else if(flag==2){
                                        printf("destination linked list empty\n");
                                }else{
                                        printf("concat successfully\n");
                                }
                              }
                                break;
                        case 4:
                                {
                                printf("source linked list\n");
                                printLL(&head1);
                                printf("destination linked list\n");
                                printLL(&head2);
                                }
                                break;
                        default :
                                printf("wrong choice\n");
                }

                printf("do you want to continue\n");
                getchar();
                scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}


























