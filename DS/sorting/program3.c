//selection sort


#include<stdio.h>

int selectionSort(int arr[],int size){
	for(int i=0;i<size-1;i++){
		int minIndex=i;
		for(int j=i+1;j<size;j++){
			if(arr[minIndex] > arr[j]){
				minIndex=j;
			}
		}
			int temp=arr[i];
			arr[i]=arr[minIndex];
			arr[minIndex]=temp;
	}

}

void main(){
	        int size;

        printf("enter the array size\n");

        scanf("%d",&size);

        int arr[size];

        printf("enter the array elements\n");

        for(int i=0;i<size;i++){

                scanf("%d",&arr[i]);
        }

        printf("the array elements are\n");

        for(int i=0;i<size;i++){

                printf("%d\t",arr[i]);
        }

        printf("\n");

	selectionSort(arr,size);
	printf("sorted array is \n");
	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}

