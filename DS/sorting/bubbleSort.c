//bubble sort




#include<stdio.h>
void bubbleSort(int arr[],int size){

	for(int i=0;i<size;i++){
		for(int j=0;j<size-1-i;j++){
			if(arr[j]>arr[j+1]){
				int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}
	}
}


void main(){
	int size;
	printf("enter the array size\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter the array elements\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("array elements are \n");

	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");


	bubbleSort(arr,size);

	printf("after bubble sort the array is given by\n");

	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}

