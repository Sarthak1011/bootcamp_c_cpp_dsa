/*Program 3.
Write a program that searches the occurrence of a particular element from
a singly linear linked list.
Input linked list: |10|->|20|->|30|->|40|->|50|->|30|->|70|
Input Enter element: 30
Output: 2 times
*/


#include<stdio.h>
#include<stdlib.h>

typedef struct demo {
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);

	node->next=NULL;
	return node;
}
void addnode(){

	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
int search(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);

	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		int flag=0;
		int count=0;
		while(temp!=NULL){
			if(temp->data==num){
				count++;
				flag=1;
			}
			temp=temp->next;
		}
		if(flag==1){
			printf("count of nodes is %d\n",count);
		}else{
			printf("data not found\n");
		}
	}
	return 0;
}

int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}
	else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}
void main(){
	char choice;

	do {
		printf("1.addnode\n");
		printf("2.search\n");
		printf("3.printLL");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addnode();
				break;
			case 2:
				search();
				break;
			case 3:
				printLL();
				break;
			default :
			           printf("wrong choice\n");
		}

	   	printf("do you want to continue\n");

		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}

