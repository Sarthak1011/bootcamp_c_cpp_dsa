#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}demo;
demo *head=NULL;
//creating nodes
demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);

	node->next=NULL;

	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
void addfirst(){

	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		node->next=head;
		head=node;
	}
}
int count(){
	demo *temp=head;
	int Count=0;
	while(temp !=NULL){
		Count++;
		temp=temp->next;
	}
//	printf("%d\n",Count);
	return Count;
}

void addatpos(){
	int Count=count();
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);

	if(pos<=Count){
	demo *node=createnode();
	demo *temp=head;
		while(pos-2){
			temp=temp->next;
			pos--;
		}
		node->next=temp->next;
		temp->next=node;
	}else{
		printf("wrong position\n");
	}
}
void printLL(){
	demo *temp=head;
	while(temp !=NULL){
		printf("|%d|",temp->data);
		temp=temp->next;
	}
	printf("\n");
}

void main(){

	char choice;
	do{
		
		printf("1.addnode\n");
		printf("2.addfirst\n");
		printf("3.addatpos\n");
		printf("4.count\n");
		printf("5.printLL\n");

		int ch;
		printf("enter your choice\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;

			case 2:
				addfirst();
				break;

			case 3:
				addatpos();
				break;

			case 4:
				count();
				break;

			case 5:
				printLL();
				break;
			default:
				printf("wrong choice\n");
		}

		printf("do you want to continue please select y or n \n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='Y' ||choice=='y' );
}


		
