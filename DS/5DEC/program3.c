//delete first node

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

void addnode(){

	demo *node=(demo *)malloc (sizeof(demo));

	printf("enter the data\n");
	scanf("%d",&node->data);

	node->next=NULL;

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}

void delete_first_node(){
	demo *temp=head;
	head=temp->next;
	free(temp);
}

void delete_last_node(){

	if(head==NULL){
		free(head);
	}else{
	demo *temp=head;
	while(temp->next!=NULL){
		temp=temp->next;
	}
	free(temp->next);
	temp->next=NULL;
	}
}

void printLL(){
	demo *temp=head;
	while(temp !=NULL){
		printf("|%d|",temp->data);
		temp=temp->next;
	}
	printf("\n");
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.delete_first_node\n");
		printf("3.printLL\n");
		printf("4.delete_last_node\n");

		int ch;
		printf("enter your choice\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				delete_first_node();
				break;
			case 3:
				printLL();
				break;
			case 4:
				delete_last_node();
				break;
			default :
			        printf("wrong choice\n");
		}

	printf("do you want to continue please enter y or n\n");
	getchar();
	scanf("%c",&choice);
	}while(choice=='y' ||choice=='Y');
}







