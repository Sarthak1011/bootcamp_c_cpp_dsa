//find peak element


#include<stdio.h>

int peakEle(int arr[],int size){

	int store=arr[0];
	for(int i=0;i<size;i++){
		if(store<arr[i]){
			store=arr[i];
		}
	}
	return store;
}
void main(){
        int size;
        printf("enter the size\n");
        scanf("%d",&size);

        int arr[size];
        printf("enter the array elements\n");
        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("array elements are\n");
        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
        printf("\n");
        int ret=peakEle(arr,size);
    	printf("peak element in the array is %d\n",ret);
      }
