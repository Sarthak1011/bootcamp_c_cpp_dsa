//rotated array


#include<stdio.h>
int rotatedArray(int arr[],int size,int key){
	int start2=0,end1=0;

	for(int i=0;i<size;i++){
		if(arr[i]<arr[i+1]){
			continue;
		}else{
			end1=i;
			start2=end1+1;
		}
	}
	int start1=0,end2=size-1;
	int mid=0;
	while(start1<=end1){
		mid=start1+(end1-start1)/2;
		if(arr[mid]==key){
			return mid;
		}
		if(arr[mid]<key){
			start1=mid+1;
		}
		if(arr[mid]>key){
			end1=mid-1;
		}
	}
	while(start2<=end2){
		mid=start2+(end2-start2)/2;
		if(arr[mid]==key){
			return mid;
		}
		if(arr[mid]<key){
			start2=mid+1;
		}
		if(arr[mid]>key){
			end2=mid-1;
		}
	}
	return -1;

}
void main(){
        int size;
        printf("enter the size\n");
        scanf("%d",&size);

        int arr[size];
        printf("enter the array elements\n");
        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("array elements are\n");
        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
        printf("\n");
        int key;
        printf("enter the key element\n");
        scanf("%d",&key);
        int ret=rotatedArray(arr,size,key);
        if(ret!=-1){
                printf("searching element in the rotated array is=%d\n",ret);
        }else{
                printf("not found\n");
        }
}

