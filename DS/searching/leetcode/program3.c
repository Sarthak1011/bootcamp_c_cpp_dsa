//first and last occurance


#include<stdio.h>

int firstOcc(int arr[],int size,int target){
	 int start=0,end=size-1,mid=-1;

	 int first=-1;
	 while(start<=end){
		 mid=start+(end-start)/2;

		 if(arr[mid]==target){
			 first=mid;
			 end=mid-1;
		 }
		 if(arr[mid]<target){
			 start=mid+1;
		 }
		 if(arr[mid]>target){
			 end=mid-1;
		 }
	 }
	 return first;

}
int lastOcc(int arr[],int size,int target){
	int start=0,end=size-1,mid=-1;
	int last=-1;
	while(start<=end){
		mid=start+(end-start)/2;

		if(arr[mid]==target){
			last=mid;
			start=mid+1;
		}
		if(arr[mid]<target){
			start=mid+1;
		}
		if(arr[mid]>target){
			end=mid-1;
		}
	}
	return last;
}

void main(){
        int size;
        printf("enter the size\n");
        scanf("%d",&size);

        int arr[size];

        printf("enter the array elements\n");

        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }

        int target;
        printf("enter the target element\n");
        scanf("%d",&target);

        int first=firstOcc(arr,size,target);
	int last=lastOcc(arr,size,target);
        if(first==-1 && last==-1){
                printf("[%d,%d]\n",first,last);
        }else{

        printf("[%d,%d]\n",first,last);
}
}
