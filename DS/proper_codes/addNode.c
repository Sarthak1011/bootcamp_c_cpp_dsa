
#include<stdio.h>
#include<stdlib.h>

typedef struct busStand{
	char sname[20];
	int nobus;
	struct busStand *next;
}bs;

bs *head=NULL;

bs *createnode(){
	bs *node=(bs *)malloc (sizeof(bs));
	getchar();
	printf("enter the bus stand name\n");
	int i=0;
	char ch;
	while((ch=getchar())!='\n'){
		(*node).sname[i]=ch;
		i++;
	}
	printf("enter the no of buses\n");
	scanf("%d",&node->nobus);
	node->next=NULL;
	return node;

}

void addnode(){
	bs *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		bs *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
void addfirst(){
		bs *node=createnode();

		if(head==NULL){
			head=node;
		}else{
			node->next=head;
			head=node;
		}
}
void addlast(){
	addnode();
}
int countnode(){
	int count=0;
	bs *temp=head;
		while(temp!=NULL){
			count++;
			temp=temp->next;
		}
	return count;
}

int addatpos(){

	int count=countnode();
	int pos;
	printf("enter the postion\n");
	scanf("%d",&pos);

	if(pos<=0 ||pos>=count+2){
		printf("wrong input\n");
		return -1;
	}else{
		if(pos==1){
			addfirst();
		}else if(pos==count+1){
			addlast();
		}else{
			bs *temp=head;
			bs *node=createnode();
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			node->next=temp->next;
			temp->next=node;
		}

	}
	return 0;
}
int printLL(){
	
	bs *temp=head;
	if(head==NULL){
		printf("no any node to print\n");
		return -1;
	}else{
	while(temp->next != NULL){
		printf("|%s->",temp->sname);
		printf("%d|->",temp->nobus);
		temp=temp->next;
	}
		printf("|%s->",temp->sname);
		printf("%d|\n",temp->nobus);
}
return 0;
}
int delfirst(){
	if(head == NULL){
		printf("no node to delete\n");
		return -1;
	}else if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			bs *temp = head;
			head=temp->next;
			free(temp);
		}
	return 0;
}
int dellast(){
	if(head==NULL){
		printf("no node to delete\n");
		return -1;
	}else if(head->next=NULL){
			free(head);
			head=NULL;
		}else{
			bs *temp=head;
			while(temp->next->next!=NULL){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
	return 0;
}
int delatpos(){
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);

	int count=countnode();

	if(pos<=0 || pos>count){
		printf("invalid position\n");
		return -1;
	}else if(pos==1){
			delfirst();
		}else if(pos==count){
			dellast();
		}else{
			bs *temp1=head,*temp2=head;
			while(pos-2){
				temp1=temp1->next;
				temp2=temp2->next;
				pos--;
			}
			temp1=temp1->next;
			temp2->next=temp2->next->next;
			free(temp1);
		}
	return 0;
}

	

void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.addfirst\n");
		printf("3.addlast\n");
		printf("4.addatpos\n");
		printf("5.printLL\n");
		
		printf("6.delfirst\n");
		printf("7.dellast\n");
		printf("8.delatpos\n");
		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				addfirst();
				break;
			case 3:
				addlast();
				break;
			case 4:
				addatpos();
				break;
			case 5:
				printLL();
				break;
			case 6:
				delfirst();
				break;
			case 7:
				dellast();
				break;
			case 8:
				delatpos();
				break;
			default :
			          printf("wrong choice\n");
		}


                      printf("do you want to continue\n");
                      getchar();
		      scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}


			

