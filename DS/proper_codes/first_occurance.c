//first ocuurance

/*Program 1.
Write a program that searches for the first occurrence of a particular
element from a singly linear linked list.
Input linked list: |10|->|20|->|30|->|40|->|50|->|30|->|70|
Input: Enter element: 30
Output : 3*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}Node;

Node *head=NULL;

Node *createNode(){
	Node *newNode=(Node *)malloc (sizeof(Node));
	printf("enter the data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	Node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

int first_Occurance(){
	if(head==NULL){
		return -1;
	}else{
		int flag=0;
	        int search;
		printf("enter the searching data\n");
		scanf("%d",&search);
		Node *temp=head;
		int count=0;
		while(temp!=NULL){
			count++;
			if(search==temp->data){
				flag=1;
				break;
      	        	}else{
				flag=0;
			}
         		temp=temp->next;
        	}
		if(flag==1){

		printf("the first_occurance of %d is %d\n",search,count);
		}else{
			printf("element not found\n");
		}
	}
	return 0;
}
int printLL(){
	if(head==NULL){
		return -1;
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}
void main(){
	char choice;

	do {
		printf("1.addnode\n");
		printf("2.first_Occurance\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				break;
			case 2:{
				int ret=first_Occurance();
				if(ret==-1){
					printf("linked is empty\n");
				}
			       }
				break;
			case 3:{
				int ret=printLL();
				if(ret==-1){
					printf("linked list is empty\n");
				}
			       }
				break;
			default :
			             printf("wrong choice\n");
       		}                           

	                 printf("do you want to continue\n");
		         getchar();
			 scanf("%c",&choice);
	}while(choice=='Y'||choice=='y');
}






