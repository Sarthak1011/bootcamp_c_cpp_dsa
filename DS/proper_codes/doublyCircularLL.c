//doublycircularlinked list
#include<stdio.h>
#include<stdlib.h>
typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}Node;

Node *head=NULL;

Node *createNode(){
	
	Node *newNode=(Node *)malloc(sizeof(Node));
	newNode->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
		head->next=head;
		head->prev=head;
	}else{
		head->prev->next=newNode;
		newNode->prev=head->prev;
		newNode->next=head;
		head->prev=newNode;
	}
}
void addFirst(){
	Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
		head->next=head;
		head->prev=head;
	}else{
		newNode->next=head;
		newNode->prev=head->prev;
		head->prev->next=newNode;
		head->prev=newNode;
		head=newNode;
	}
}
void addLast(){
	addNode();
}
int countNode(){
	int count=0;
	Node *temp=head;
	while(temp->next!=head){
		count++;
		temp=temp->next;
	}
	count++;
	return count;
}

int addAtPos(int pos){
	int count=countNode();
		if(pos<=0 || pos>=count+2){
			return -1;
		}else{
			if(pos==1){
				addFirst();
			}else if(pos==count+1){
				addLast();
			}else{
				Node *newNode=createNode();
				Node *temp=head;
				while(pos-2){
					temp=temp->next;
					pos--;
				}
				newNode->next=temp->next;
				newNode->prev=temp;
				temp->next->prev=newNode;
				temp->next=newNode;
			}
		}
		return 0;
}
int deleteFirst(){
	if(head==NULL){
		return -1;
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			head=head->next;
			head->prev=head->prev->prev;
			free(head->prev->next);
			head->prev->next=head;
		}
		return 0;
	}
}
int deleteLast(){
	if(head==NULL){
		return -1;
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
		
			head->prev=head->prev->prev;
			free(head->prev->next);
			head->prev->next=head;
		}
		return 0;
	}
}

int deleteAtPos(int pos){
	if(head==NULL){
		return -1;
	}else{
	int count=countNode();
	if(pos<=0||pos>count){
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			Node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
		return 0;
	}
	}
}
int printLL(){
	if(head==NULL){
		return -1;
	}else{
		Node *temp=head;
		while(temp->next!=head){
			printf("|%d|->",temp->data);
			temp=temp->next;

		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}



void main(){


        char choice;

        do {
                printf("!....*Your choice*....!\n");
                printf("1.addNode\n");
                printf("2.addFirst\n");
                printf("3.addLast\n");
                printf("4.addAtPos\n");
                printf("5.deletsFirst\n");
                printf("6.deleteLast\n");
                printf("7.deleteAtPos\n");
                printf("8.printLL\n");

                int ch;
                printf("enter your choice\n");
                scanf("%d",&ch);

                switch(ch){

                        case 1:
                                addNode();
                                printLL();
                                break;
                        case 2:
                                addFirst();
                                printLL();
                                break;
                        case 3:
                                addLast();
                                printLL();
                                break;
                        case 4:{
                               int pos;
                               printf("enter the postion of node\n");
                               scanf("%d",&pos);
                               int ret=addAtPos(pos);
                               if(ret==-1){
                                       printf("you entered wrong position\n");
                               }
                               }
                                printLL();
                                break;
                        case 5:{
                                int ret=deleteFirst();
                                if(ret == -1){
                                        printf("There is no node to delete\n");
                                }
                               }
                                printLL();
                                break;
                        case 6:{
                                int ret=deleteLast();
                                if(ret==-1){
                                        printf("There is no to delete\n");
                                }
                               }
                                printLL();
                                break;
                        case 7:{
                                       int pos;
                                       printf("enter the postion\n");
                                       scanf("%d",&pos);

                                int ret=deleteAtPos(pos);
                                if(ret==-1){
                                        printf("There is no node to delete\n");
                                }
                               }
                                printLL();
                                break;
                        case 8:{
                                int ret=printLL();
                                if(ret==-1){
                                        printf("linked list is empty\n");
                                }
                               }

                                break;
                        default :
                                     printf("wrong choice\n");
                }


                printf("do you want to continue\n");
                getchar();
                scanf("%c",&choice);
        }while(choice=='Y'||choice=='y');
}
