//prime numbers
#include<stdio.h>
#include<stdbool.h>
bool prime(int num){
	int size=num/2;
	for(int i=0;i<size;i++){
		if(num>2 && num%2==0){
			return false;
		}else{
			return true;
		}
	}

}

void main(){

	int num;
	printf("enter the number to check the prime or not\n");
	scanf("%d",&num);

	bool result=prime(num);

	if(result==true){
		printf("prime number\n");
	}else{
		printf("not prime number\n");
	}
}
