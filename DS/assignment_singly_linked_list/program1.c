/*
 Program 1. Write a program that searches all occurrences of a
particular element from a singly linear linked list.
Input linked list : |10|->|20|->|30|->|40|->|30|->|30|->|70|
Input element: 30
Output : 3
 */ 
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}Node;

Node *head=NULL;

Node *createNode(){
	Node *newNode=(Node *)malloc(sizeof(Node));
	printf("enter the data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	Node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int all_Occurences(){
	int count=0,cnt=0;
	if(head==NULL){
		return -1;
	}else{
		int search;
		int x=0,y=0;
		printf("Input element:\n");
		scanf("%d",&search);
		Node *temp=head;
		while(temp!=NULL){
			cnt++;
			if(search==temp->data){
				//x=cnt;
				//y=x;
				count++;
				
			}
			temp=temp->next;
		}
		if(count==1){
			printf("first Occurenece of %d is %d\n",search,cnt);
		}else if(count==2){
			printf("second Occurence x:%d,y:%d\n",x,y);
		}else if(count==3){
			printf("third Occurenece\n");
		}else{
			printf("more than 3\n");
		}
	}
	return 0;
}
int printLL(){
	if(head==NULL){
		return -1;
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|",temp->data);
	}
	return 0;
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.all_Occurences\n");
		printf("3.printLL\n");


		int ch;
		printf("enter your choice\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addNode();
				printLL();
				break;
			case 2:
				all_Occurences();
				printLL();
				break;
			case 3:
				printLL();
				break;
			default :
			        printf("wrong choice\n");
		}                              

		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='Y'||choice=='y');
}



