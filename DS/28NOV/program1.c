#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{

	int empId;
	char empName[20];
	float sal;
	struct Employee *next;
}emp;

void main(){

	emp *Emp1=(emp *)malloc(sizeof(emp));
	emp *Emp2=(emp *)malloc(sizeof(emp));
	emp *Emp3=(emp *)malloc(sizeof(emp));

	Emp1->empId=1;
	strcpy(Emp1->empName,"kanha");
	Emp1->sal=20.2;
	Emp1->next=Emp2;
	
	Emp2->empId=2;
	strcpy(Emp2->empName,"rahul");
	Emp2->sal=30.2;
	Emp2->next=Emp3;
	
	Emp3->empId=3;
	strcpy(Emp3->empName,"ashish");
	Emp3->sal=40.2;
	Emp3->next=NULL;


	printf("ID:%d\n",Emp1->empId);
	printf("NAME:%s\n",Emp1->empName);
	printf("SALARAY:%f\n",Emp1->sal);
	printf("%p\n",Emp1->next);

	printf("\n");

	printf("ID:%d\n",Emp2->empId);
	printf("NAME:%s\n",Emp2->empName);
	printf("SALARAY:%f\n",Emp2->sal);
	printf("%p\n",Emp2->next);
	printf("\n");
	
	printf("ID:%d\n",Emp3->empId);
	printf("NAME:%s\n",Emp3->empName);
	printf("SALARAY:%f\n",Emp3->sal);
	printf("%p\n",Emp3->next);
}
