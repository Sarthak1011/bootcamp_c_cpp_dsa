///QUEUE using linked list


#include<stdio.h>
#include<stdlib.h>

typedef struct demo {
	int data;
	struct demo *next;
}demo;

demo *head=NULL;
int size=0;
int flag=0;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){

	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}

int countnode(){
	int count=0;
	demo *temp=head;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	return count;
}
int enqueue(){
	if(countnode()==size){
		return -1;
	}else{
		addnode();
		return 0;
	}
}
int dequeue(){
	int data;
	if(head==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		if(head->next==NULL){
			data=head->data;
			free(head);
			head=NULL;
		}else{
		        data=head->data;
			demo *temp=head;
			head=head->next;
			free(temp);
		}
		return data;
	}
}
int frontt(){
	if(head==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		return head->data;
	}
}
int printQueue(){
	if(head==NULL){
		return -1;
	}else{
		flag=1;
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}

void main(){
	printf("enter the size\n");
	scanf("%d",&size);

	char choice;

	do{
		printf("1.enqueue\n");
		printf("2.dequeue\n");
		printf("3.frontt\n");
		printf("4.printQueue\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:{
				int ret=enqueue();
				if(ret==-1){
					printf("queue is overflow\n");
				}
			       }
			       break;
			case 2:{
				       int ret=dequeue();
				       if(flag==0){
					       printf("queue underflow\n");
				       }else{
					       printf("%d is dequed\n",ret);
				       }
			       }
			       break;
			case 3:
			         {
				  int ret=frontt();

			               if(ret==-1){
					       printf("queue underflow\n");
				       }else{
					       printf("front=%d\n",ret);
				       }
				 }
				 break;
			case 4:
				 {
					 int ret=printQueue();
					if(flag==0){
					       printf("queue is empty\n");
					}
				 }
		                  break;
			default : 
				   printf("wrong choice\n");
		}

		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}

	
			                	  






