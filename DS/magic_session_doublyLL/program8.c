/*
 Program 8.
Write a program that accepts a doubly linked list from the user.
Take a number from the user and keep the elements equal in length to that
number and delete other data elements. And print the Linked list
Length of Shashi = 6
Input: linked list: |Shashi |-> | Ashish|-> |Kanha |-> | Rahul |-> | Badhe |
Input: Enter Length 6
Output : linked list: |Shashi |-> | Ashish|
 */ 

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	struct demo *prev;
	char str[20];
	struct demo *next;
}demo;

demo *head=NULL;
int mystrlen(char *str){
	int count=0;
	while(*str!='\0'){
		count++;
		str++;;
	}
	return count;
}

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	node->prev=NULL;
	printf("enter the string\n");
	fgets(node->str,20,stdin);
	node->str[mystrlen(node->str)-1]='\0';
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
	}
}
int delete_string(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		int len;
		printf("enter the length\n");
		scanf("%d",&len);
		demo *temp=head;
		while(temp!=NULL){
			if(temp->next!=NULL){		
				if(len==mystrlen(temp->str)){
					printf("|%s|->",temp->str);
		        	}else{
		                	printf("|%s|",temp->str);
			       }
			}else{
				free(temp->str);
	         	}
			temp=temp->next;

		}
	}
	getchar();
	return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%s|->",temp->str);
			temp=temp->next;
		}
			printf("|%s|",temp->str);
	}
	getchar();
	return 0;
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.delete_string\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				delete_string();
				break;
			case 3:
				printLL();
				break;
			default :
			         printf("wrong choice\n");
		}	

	           

	printf("do you want to continue\n");
        scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}





			
