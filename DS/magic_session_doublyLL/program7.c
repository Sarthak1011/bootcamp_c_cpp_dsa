/*
 Program 7.
Write a program that accepts a doubly linked list from the user.
Reverse the data elements from the linked list.
Submit with a proper diagram.
Input: linked list: |Shashi |-> | Ashish|-> |Kanha |-> | Rahul |-> | Badhe |
Output : linked list |ihsahS|-> |hsihsA|->|ahnaK|->|luhaR|->|ehdaB|
 */ 

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct demo{
	struct demo *prev;
	char str[20];
	struct demo *next;
}demo;

demo *head=NULL;

char *strrev(char *str){
	char *temp=str;
	while(*temp!='\0'){
		temp++;
	}
	temp--;
	while(str<temp){
		int x=*str;
		*str=*temp;
		*temp=x;
		str++;
		temp--;
	}
	return str;
}
demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	node->prev=NULL;
	printf("enter the string\n");
	fgets(node->str,20,stdin);
	node->str[strlen(node->str)-1]='\0';
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
		}
}
int string_reverce(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp!=NULL){
			strrev(temp->str);

			if(temp->next!=NULL){
				printf("|%s|->",temp->str);
				}else{

				printf("|%s|\n",temp->str);
				}
				temp=temp->next;
		
		}
	}
	getchar();
	return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%s|->",temp->str);
			temp=temp->next;
		}
			printf("|%s|",temp->str);
	}
	getchar();
	return 0;
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.string_reverce\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				string_reverce();
				break;
			case 3:
				printLL();
				break;
			default :	
				printf("wrong choice\n");
		}

		printf("do you want to continue\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
