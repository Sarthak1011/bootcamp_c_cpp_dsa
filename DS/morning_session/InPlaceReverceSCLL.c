//inplace reverce using singly circluar linked list 
//
#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);

	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();
	if(head==NULL){
		head=node;
		head->next=head;
	}else{
		demo *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		temp->next=node;
		node->next=head;
	}
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=head){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;

}

int InPlaceReverceSCLL(){

	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp1=NULL,*temp2=NULL,*temp3=head;
		while(temp2!=head){
			temp2=temp3->next;
			temp3->next=temp1;
			temp1=temp3;
			temp3=temp2;
		}
		head->next=temp1;
		head=temp1;
	}
	return 0;
}
void main(){
	printf("enter the node count\n");
	int nodecount;
	scanf("%d",&nodecount);

	for(int i=0;i<nodecount;i++){
		addnode();
		printLL();
	}
	InPlaceReverceSCLL();
	printLL();
}



