//palindrome node

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct demo {
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}

int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}

int countnode(){
	if(head==NULL){
		printf("linked list empty\n");
		return -1;
	}else{
		demo *temp=head;
		int count=0;
		while(temp!=NULL){
			count++;
			temp=temp->next;
		}
		return count;
	}
}

int palindrome(){
	int count=countnode();
	int arr[count];

	int i=0;
	demo *temp=head;
	while(temp!=NULL){
		arr[i]=temp->data;
		i++;
		temp=temp->next;
	}

	int start=0;
	int end=count-1;


	while(start<end){
		if(arr[start]!=arr[end]){
			return false;
		}else{
		start++;
		end--;
		return true;
	}
}
}
void main(){
	char choice;

	do {
		printf("1.addnode\n");
		printf("2.palindrome1\n");
		printf("3.printLL\n");
		printf("4.palindrome2\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addnode();
				printLL();
				break;
			case 2:
				{
					bool ret=palindrome();
					if(ret){
					printf("palindrome\n");
					}else{
						printf("not palindrome\n");
					}
				}
				break;

			case 3:
			         printLL();
			         break;
			/*case 4: 
		               break;
*/
			default :
			           printf("wrong choice\n");
		}

                             
                          printf("do you want to continue\n");
		          getchar();
		          scanf("%c",&choice);
	}while(choice=='Y'||choice=='y');
}

