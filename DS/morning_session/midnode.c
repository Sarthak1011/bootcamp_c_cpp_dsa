//find middle node from the linked list approch 1

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}demo;
int flag=0;

demo *head=NULL;

demo *createnode(){

	demo *node=(demo *)malloc(sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);

	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
int countnode(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		int count=0;
		while(temp!=NULL){
			temp=temp->next;
			count++;
		}
		return count;
	}
}
int midnode1(){
	int count=countnode();
	if(count==1){
		flag=0;
		return -1;	

	}else if(count==2){
		flag=1;
		return -1;
	}else{

	int cnt=count/2;
	demo *temp=head;
	while(cnt-1){
		flag=2;
		temp=temp->next;
		cnt--;
	}
	return temp->data;
}
}
int midnode2(){
	if(head==NULL){
		flag=0;
		return -1;
	}else if(head->next==NULL){
		flag=1;
		return -1;
	}else if(head->next->next==NULL){
		flag=2;
		return -1;
	}else{
		flag=3;

	demo *fastptr=head->next;
	demo *slowptr=head;

	while(fastptr!=NULL && fastptr->next!=NULL){
		fastptr=fastptr->next->next;
		slowptr=slowptr->next;
	}
	return slowptr->data;
}
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}
void main(){
	char choice;

	do {
		printf("1.addnode\n");
		printf("2.midnode1\n");
		printf("3.printLL\n");
		printf("4.midnode2\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addnode();
				printLL();
				break;
			case 2:{

			       int ret=midnode1();
			       if(flag==0){
				       printf("only one node is present\n");
			       }else if(flag==1){
				       printf("two nodes are present no take mid\n");
			       }else{

			       printf("midnode of linked list is %d\n",ret);
			       }
			       }
			        break;

			case 3:
			         printLL();
			         break;
			case 4: 
				 {
					int ret=midnode2();
					if(flag==0){
						printf("linked list is empty\n");
					}else if(flag==1){
						printf("linked list have one node\n");
					}else if(flag==2){
						printf("linked list have two nodes\n");
					}else{
						printf("mid node of linked list is %d\n",ret);
					}
				}
		                break;

			default :
			           printf("wrong choice\n");
		}

                             
                          printf("do you want to continue\n");
		          getchar();
		          scanf("%c",&choice);
	}while(choice=='Y'||choice=='y');
}	

