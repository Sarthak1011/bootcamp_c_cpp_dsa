//ftell and rewind
//

#include<stdio.h>
void main(){
	FILE *fp=fopen("info.txt","w+");

	printf("%ld\n",ftell(fp));//0
	fprintf(fp,"programmer");

	printf("%ld\n",ftell(fp));//10

	fprintf(fp,"coding");
	printf("%ld\n",ftell(fp));//16

	rewind(fp);//reverse to starting

	fprintf(fp,"study");
	printf("%ld\n",ftell(fp));//5

}
